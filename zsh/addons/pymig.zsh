
function mig() {
    python manage.py schemamigration "$@" --auto;
    python manage.py migrate "$@";
}
function miginit() {
    python ./manage.py schemamigration --initial "$@"
}
