function djget() {
djvused $1 -e "print-outline" > /tmp/$1.tmp
ascii2uni -q -a K < /tmp/$1.tmp > $1.toc
}

function djg() {
djvused $1 -e "print-outline" > /tmp/$1.tmp
ascii2uni -q -a K < /tmp/$1.tmp > $1.toc
}
function djs() {
for a in $@; do
    print $a
	djvused $a -s -e "set-outline $a.toc"
done
}
