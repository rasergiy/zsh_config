
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"
alias irbi='ruby --enable-gems -S irb'
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
