alias fixx='xhost +local:'
alias mv='nocorrect mv'       # no spelling correction on mv
alias cp='nocorrect cp'       # no spelling correction on cp
alias mkdir='nocorrect mkdir' # no spelling correction on mkdir

alias -g COLER='2>>( sed -ue "s/.*/$fg_bold[red]&$reset_color/" 1>&2 )'		# Usage : find /etc COLER  will highlight all errors

alias mpl=' mplayer -keepaspect -vo x11'	# mplayer with keep aspect option

alias mc='USER_MCDIR=/tmp/`id | sed "s/[^(]*(//;s/).*//"`-mcpwd; rm $USER_MCDIR -f; mc -x -P $USER_MCDIR; cd `cat $USER_MCDIR`; rm $USER_MCDIR -f'

alias say='voiceman --say'

alias dud="(setopt globdots;du -ks * | sort -nr | sed -e 's/^\([0-9]\{1,\}\)[0-9]\{6\}[^0-9]/\1G\t/;t;s/^\([0-9]\{1,\}\)[0-9]\{3\}[^0-9]/\1M\t/;t;d')"
alias phplog="sudo tail -f /var/log/apache2/error.log | ccze --plugin php"

alias wgetr='wget --recursive --dont-remove-listing --level=inf --convert-links --execute robots=off --user-agent="Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FunWebProducts)"'
alias ls='ls --color=always'
alias lsl='ls --full-time --color=always'
alias la='ls  --color=always -la  --full-time'

alias df='df -h'
alias du1='du --human-readable --max-depth=1'
alias scra='screen -r'
alias scrd='screen -dr'
alias svim='sudo vim'
alias du='du --human-readable'
alias j=jobs
alias pu=pushd
alias po=popd
alias d='dirs -v'
alias h=history
alias grep=egrep

